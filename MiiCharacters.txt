Mii characters
written by Jonathan Phillips
ECE112 P&P Git Lab

The Mii characters are customizable persons used on Nintendo's Wii Consul. 
They can be made to look like the person that is controlling them. 
There are numerous skin tones, body sizes, body shapes, and facial structures allowed. 
The person playing the Wii can use their Mii in numerous game to add a little customization to their fun. 

